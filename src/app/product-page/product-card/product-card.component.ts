import {Component, Input, OnDestroy, OnInit, EventEmitter, Output} from '@angular/core';
import {Product} from '../../core/models/product';
import {ProductService} from '../../core/services/product.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProductAddPageReactiveFormComponent} from '../product-add-page-reactive-form/product-add-page-reactive-form.component';
import {ProductMode} from '../../core/enums/product-mode.enum';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit, OnDestroy {
  @Output() productRemoved = new EventEmitter<number>();
  @Input() product: Product;
  @Input() index;
  @Input() odd;
  @Input() even;
  @Input() first;
  @Input() last;
  show = false;

  constructor(
    private productService: ProductService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    console.log('ngOnInit', this.product.name);
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy', this.product.name);
  }

  toggle(): void {
    this.show = !this.show;
  }

  remove(id: number): void {
    if (confirm('Czy na pewno usunąć?')) {
      this.productService.deleteProductById(id)
        .subscribe(
          () => this.productRemoved.emit(id),
          () => alert('Produkt nie mógł zostać usunięty'),
        );
    }
  }

  editModalOpen(product: Product): void {
    const modalRef = this.modalService.open(ProductAddPageReactiveFormComponent);
    modalRef.componentInstance.mode = ProductMode.UPDATE; // @Input w ProductAddPageReactiveFormComponent
    modalRef.componentInstance.product = product; // @Input w ProductAddPageReactiveFormComponent
    modalRef.componentInstance.productChange // @Output w ProductAddPageReactiveFormComponent
      .subscribe((updatedProduct: Product) => {
        Object.assign(product, updatedProduct); // przepisanie obiektu 2 do obiektu 1
        modalRef.close(); // zamyka okno
      });
  }
}
