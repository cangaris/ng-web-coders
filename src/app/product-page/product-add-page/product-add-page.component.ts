import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-product-add-page',
  templateUrl: './product-add-page.component.html',
  styleUrls: ['./product-add-page.component.css']
})
export class ProductAddPageComponent implements OnInit {
  initialName = 'Damian';
  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(ngForm: NgForm): void {
    console.log(ngForm);
  }
}

// TemplateDriven
// ReactiveForms
