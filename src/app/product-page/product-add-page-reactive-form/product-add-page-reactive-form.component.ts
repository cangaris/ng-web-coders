import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProductService} from '../../core/services/product.service';
import {ProductMode} from '../../core/enums/product-mode.enum';
import {Product} from '../../core/models/product';
import {Utils} from '../../core/utils/utils';

@Component({
  selector: 'app-product-add-page-reactive-form',
  templateUrl: './product-add-page-reactive-form.component.html',
  styleUrls: ['./product-add-page-reactive-form.component.css']
})
export class ProductAddPageReactiveFormComponent implements OnInit {

  @Output() productChange = new EventEmitter<Product>();
  @Input() product: Product;
  @Input() mode = ProductMode.CREATE;
  productMode = ProductMode;
  form: FormGroup;

  constructor(private fb: FormBuilder, private service: ProductService) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      id: this.product?.id,
      name: [this.product?.name, [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      price: [this.product?.price, [
        Validators.required,
        Validators.min(1), Validators.max(100_000),
        Validators.pattern(Utils.INTEGER),
      ]],
      weight: [this.product?.weight, [Validators.required, Validators.min(0.1), Validators.max(100)]],
      image: [this.product?.image, [Validators.required]],
      date: [this.product?.date, [Validators.required]],
    });
  }

  /**
   * DRY - don't repeat yourself - dobra praktyka nie powtarzania się
   * czyli jeżeli widzimy, że dublujemy dany kawałek kodu
   * oznacza, że łamiemy DRY i powinniśmy pomyśleć nad optymalizacją
   */
  onSubmit(): void {
    if (this.form.valid) {
      const result = this.mode === ProductMode.UPDATE ?
        this.service.updateProduct(this.form.value) :
        this.service.createProduct(this.form.value);
      result.subscribe(
        product => {
            this.productChange.emit(product);
            alert('Operacja zakończona sukcesem!');
          },
        error => alert('Operacja zakończona niepowodzeniem!'),
      );
    } else {
      alert('Formularz jest niepoprawny!');
    }
  }
}
