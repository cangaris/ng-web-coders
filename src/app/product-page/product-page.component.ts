import { Component, OnInit } from '@angular/core';
import {Product} from '../core/models/product';
import {ProductService} from '../core/services/product.service';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css']
})
export class ProductPageComponent implements OnInit {

  products: Product[] = [];

  constructor(private service: ProductService) {}

  ngOnInit(): void {
    this.service.getProducts()
      .subscribe((value: Product[]) => this.products = value);
  }

  getById(): void {
    const id = prompt('Podaj ID produktu:');
    this.service.getProductById(+id)
      .subscribe(
        (value: Product) => alert(`Wybrano: ${value.name}`),
        error => alert(`Ups! Wystąpił błąd, podałeś błędne ID`),
      );
  }

  catchProduct(id: number): void {
    const foundIndex = this.products.findIndex(value => value.id === id);
    this.products.splice(foundIndex, 1);
  }
}
