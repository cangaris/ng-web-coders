import {Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-child2',
  templateUrl: './child2.component.html',
  styleUrls: ['./child2.component.css']
})
export class Child2Component {

  @Output() textChange = new EventEmitter<string>(); // komunikacja dziecko - rodzic

  catchText(text: string): void {
    this.textChange.emit(text); // emisja zdarzenia do rodzica
  }
}
