import {Directive, ElementRef, HostListener, OnInit} from '@angular/core';

@Directive({
  selector: '[appFont]'
})
export class FontDirective {

  constructor(private el: ElementRef) { }

  @HostListener('mouseenter')
  onMouseEnter(): void {
    const styles = 'font-size: 20px; color: blue; background-color: yellow;';
    this.el.nativeElement.setAttribute('style', styles);
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.el.nativeElement.removeAttribute('style');
  }
}
