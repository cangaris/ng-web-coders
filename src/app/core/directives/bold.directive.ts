import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appBold]'
})
export class BoldDirective {

  constructor(private el: ElementRef) { }

  @HostListener('mouseenter')
  onMouseEnter(): void {
    const styles = 'font-weight: bold;';
    this.el.nativeElement.setAttribute('style', styles);
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.el.nativeElement.removeAttribute('style');
  }
}
