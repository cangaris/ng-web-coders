import {Directive, ElementRef, OnInit} from '@angular/core';

@Directive({
  selector: '[appTextColor]'
})
export class TextColorDirective implements OnInit {

  constructor(private element: ElementRef) {
  }

  ngOnInit(): void {
    this.element.nativeElement.setAttribute('style', 'color: red');
  }
}
