import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'creditCard'
})
export class CreditCardPipe implements PipeTransform {

  transform(value: string, ...args: string[]): string {
    if (value.length === 19) {
      const stars = '*'.repeat(4);
      return `${value.substr(0, 4)} ${stars} ${stars} ${value.substr(15, 4)}`;
    }
    return value;
  }
}
