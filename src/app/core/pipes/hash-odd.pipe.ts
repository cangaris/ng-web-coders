import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hashOdd'
})
export class HashOddPipe implements PipeTransform {

  transform(value: string, ...args: string[]): string {
    return value // "damian"
      .split('') // ['d', 'a', 'm', 'i', 'a', 'n']
      .map((el, index) => index % 2 ? '*' : el ) // jeśli parzysty to * jeśli nie to brak zmiany
      .join(''); // połącz ['d', '*', 'm', '*', 'a', '*'] w "d*m*a*n"
  }
}
