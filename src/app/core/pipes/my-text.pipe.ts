import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myText'
})
export class MyTextPipe implements PipeTransform {

  transform(value: string, ...args: string[]): string {
    if (args[0] === 'U') {
      return value.toUpperCase();
    }
    if (args[0] === 'L') {
      return value.toLocaleLowerCase();
    }
    return value;
  }
}
