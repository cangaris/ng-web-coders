import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'phoneProtection'
})
export class PhoneProtectionPipe implements PipeTransform {

  transform(value: string, ...args: string[]): string {
    if (value.length > 4) {
      const stars = '*'.repeat(value.length - 4);
      return `${value.substr(0, 2)}${stars}${value.substr(value.length - 2)}`;
    }
    return value;
  }
}
