import { Pipe, PipeTransform } from '@angular/core';
import {split} from 'ts-node';

@Pipe({
  name: 'emailProtection'
})
export class EmailProtectionPipe implements PipeTransform {

  transform(value: string, ...args: string[]): string {
    // const parts = value.split(/@|\./);
    // const emailName = parts[0];
    // const emailCompanyDomain = parts[1];
    // const emailCountryDomain = parts[2];
    // poniżej szybszy zapis 1 1 linijce to samo co 4 linijki powyżej (destruktor tablic)
    const [emailName, emailCompanyDomain, emailCountryDomain] = value.split(/@|\./);
    const hashedEmailName = emailName.charAt(0) + '*'.repeat(emailName.length - 1);
    const lastChar = emailCompanyDomain.charAt(emailCompanyDomain.length - 1);
    const hashedCompanyDomain = '*'.repeat(emailCompanyDomain.length - 1) + lastChar;
    return `${hashedEmailName}@${hashedCompanyDomain}.${emailCountryDomain}`;
  }
}
