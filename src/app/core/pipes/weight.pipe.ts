import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'weight'
})
export class WeightPipe implements PipeTransform {

  transform(value: number, ...args: string[]): string {
    const mode = args[0];
    if (mode && mode === 'g') {
      return (+value * 1000) + ' g';
    }
    return value + ' kg';
  }
}
