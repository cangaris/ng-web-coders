import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl} from '@angular/forms';

@Component({
  selector: 'app-error-label',
  templateUrl: './error-label.component.html',
  styleUrls: ['./error-label.component.css']
})
export class ErrorLabelComponent implements OnInit {

  @Input() control: AbstractControl;

  ngOnInit(): void {
    // możliwość subskrypcji na zmianę wartości kontrolki
    // this.control.valueChanges
    //   .subscribe(value => console.log('zmieniona wartość na:', value));
    //
    // możliwość subskrypcji na zmianę walidacji kontrolki (VALID/INVALID)
    // this.control.statusChanges
    //   .subscribe(value => console.log('zmieniony status walidacji na:', value));
  }
}
