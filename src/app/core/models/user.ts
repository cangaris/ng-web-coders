export class User {
  id: number;
  firstName: string;
  lastName: string;
  age: number;
  city: string;
  email: string;
  phone: string;
  image: string;
  title: string;
}
