export class Product {
  id: number;
  name: string;
  price: number;
  weight: number;
  image: string;
  date: string;
}
