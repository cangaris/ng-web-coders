import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RxJsService {

  // siblingsSubject = new Subject();
  private readonly siblingsSubject = new BehaviorSubject('nie kilknięte');

  getSiblingsSubject(): Observable<string> {
    return this.siblingsSubject.asObservable()
      .pipe(
        map(value => value + '!'),
        map(value => value + '?'),
      );
  }

  setSiblingsSubject(value: string): void {
    this.siblingsSubject.next(value);
  }
}
