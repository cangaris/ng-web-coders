import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly userUrl = `${environment.apiUrl}/user`;

  constructor(private http: HttpClient) {}

  /**
   * CRUD:
   * C - create
   * R - read / retrieve
   * U - update
   * D - delete
   */
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl);
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(`${this.userUrl}/${id}`);
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(this.userUrl, user);
  }

  updateUser(user: User): Observable<User> {
    return this.http.put<User>(`${this.userUrl}/${user.id}`, user);
  }

  deleteUserById(id: number): Observable<User> {
    return this.http.delete<User>(`${this.userUrl}/${id}`);
  }
}
