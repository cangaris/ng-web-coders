import {Component, Input, OnDestroy, OnInit, Output, EventEmitter} from '@angular/core';
import {UserService} from '../../core/services/user.service';
import {User} from '../../core/models/user';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserModeEnum} from '../../core/enums/user-mode.enum';
import {UserAddPageReactiveFormComponent} from '../user-add-page-reactive-form/user-add-page-reactive-form.component';

@Component({
  selector: 'app-business-card',
  templateUrl: './business-card.component.html',
  styleUrls: ['./business-card.component.css']
})
export class BusinessCardComponent implements OnInit, OnDestroy {
  @Output() removedUser = new EventEmitter<number>();
  @Input() user: User;
  @Input() first;
  @Input() last;
  show = false;

  constructor(
    private userService: UserService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    console.log('ngOnInit', this.user.firstName);
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy', this.user.firstName);
  }

  toggle(): void {
    this.show = !this.show;
  }

  remove(id: number): void {
    if (confirm('Czy na pewno usunąć?')) {
      this.userService.deleteUserById(id)
        .subscribe(
          () => this.removedUser.emit(id),
          () => alert('Użytkownik nie mógł zostać usunięty!'),
        );
    }
  }

  editModalOpen(user: User): void {
    const modalRef = this.modalService.open(UserAddPageReactiveFormComponent);
    modalRef.componentInstance.mode = UserModeEnum.UPDATE;
    modalRef.componentInstance.user = user;
    modalRef.componentInstance.userChange
      .subscribe((updatedUser: User) => {
        Object.assign(user, updatedUser);
        modalRef.close();
      });
  }
}
