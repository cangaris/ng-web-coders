import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../core/models/user';
import {UserService} from '../core/services/user.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  users: User[] = [];

  constructor(private service: UserService) {}

  ngOnInit(): void {
    this.service.getUsers()
      .subscribe((value: User[]) => this.users = value);
  }

  getById(): void {
    const id = prompt('Podaj ID użytkownika:');
    this.service.getUserById(+id)
      .subscribe(
        (value: User) => alert(`Wybrano: ${value.firstName}`),
        error => alert(`Ups! Wystąpił błąd, podałeś błędne ID`),
      );
  }

  catchUser(id: number): void {
    const foundIndex = this.users.findIndex(value => value.id === id);
    this.users.splice(foundIndex, 1);
  }
}
