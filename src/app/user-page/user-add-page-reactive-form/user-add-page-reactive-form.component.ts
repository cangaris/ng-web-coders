import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {UserModeEnum} from '../../core/enums/user-mode.enum';
import {User} from '../../core/models/user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../core/services/user.service';
import {Utils} from '../../core/utils/utils';

@Component({
  selector: 'app-user-add-page-reactive-form',
  templateUrl: './user-add-page-reactive-form.component.html',
  styleUrls: ['./user-add-page-reactive-form.component.css']
})
export class UserAddPageReactiveFormComponent implements OnInit {

  @Output() userChange = new EventEmitter<User>();
  @Input() user: User;
  @Input() mode = UserModeEnum.CREATE;
  userModeEnum = UserModeEnum;
  form: FormGroup;

  constructor(private fb: FormBuilder, private service: UserService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: this.user?.id,
      firstName: [this.user?.firstName, [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      lastName: [this.user?.lastName, [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      age: [this.user?.age, [Validators.required, Validators.min(18), Validators.max(100), Validators.pattern(Utils.INTEGER)]],
      city: [this.user?.city, [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      email: [this.user?.email, [Validators.required, Validators.minLength(3), Validators.maxLength(30), Validators.email]],
      phone: [this.user?.phone, [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      image: [this.user?.image, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]],
      title: [this.user?.title, [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.service.updateUser(this.form.value).subscribe(
        user => {
          this.userChange.emit(user);
          alert('Operacja zakończona sukcesem!');
        },
        errorResponse => {
          const errorMessage = errorResponse.error.message;
          alert(`Operacja zakończona niepowodzeniem! ${errorMessage}`);
        },
      );
    } else {
      alert('Formularz jest niepoprawny!');
    }
  }
}
