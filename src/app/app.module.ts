import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { BusinessCardComponent } from './user-page/bussiness-card/business-card.component';
import { RouterModule } from '@angular/router';
import { UserPageComponent } from './user-page/user-page.component';
import { ProductPageComponent } from './product-page/product-page.component';
import {ProductCardComponent} from './product-page/product-card/product-card.component';
import localePl from '@angular/common/locales/pl';
import localeExtraPl from '@angular/common/locales/extra/pl';
import {registerLocaleData} from '@angular/common';
import { WeightPipe } from './core/pipes/weight.pipe';
import { MyTextPipe } from './core/pipes/my-text.pipe';
import { CreditCardPipe } from './core/pipes/credit-card.pipe';
import { EmailProtectionPipe } from './core/pipes/email-protection.pipe';
import { PhoneProtectionPipe } from './core/pipes/phone-protection.pipe';
import { HashOddPipe } from './core/pipes/hash-odd.pipe';
import { TextColorDirective } from './core/directives/text-color.directive';
import { FontDirective } from './core/directives/font.directive';
import { BoldDirective } from './core/directives/bold.directive';
import {HttpClientModule} from '@angular/common/http';
import { TestPageComponent } from './test-page/test-page.component';
import { ParentComponent } from './test-page/parent/parent.component';
import { ChildComponent } from './test-page/child/child.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Child2Component } from './test-page/child2/child2.component';
import { Parent2Component } from './test-page/parent2/parent2.component';
import {TestGuard} from './core/guards/test.guard';
import { ProductAddPageComponent } from './product-page/product-add-page/product-add-page.component';
import {ProductAddPageReactiveFormComponent} from './product-page/product-add-page-reactive-form/product-add-page-reactive-form.component';
import {ErrorLabelComponent} from './core/components/error-label/error-label.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserAddPageReactiveFormComponent } from './user-page/user-add-page-reactive-form/user-add-page-reactive-form.component';
import { RxJsPageComponent } from './rx-js-page/rx-js-page.component';
import { Sibling1Component } from './rx-js-page/sibling1/sibling1.component';
import { Sibling2Component } from './rx-js-page/sibling2/sibling2.component';

registerLocaleData(localePl, localeExtraPl);

@NgModule({
  declarations: [
    AppComponent,
    BusinessCardComponent,
    ProductCardComponent,
    UserPageComponent,
    ProductPageComponent,
    WeightPipe,
    CreditCardPipe,
    MyTextPipe,
    EmailProtectionPipe,
    PhoneProtectionPipe,
    HashOddPipe,
    TextColorDirective,
    FontDirective,
    BoldDirective,
    TestPageComponent,
    ParentComponent,
    ChildComponent,
    Child2Component,
    Parent2Component,
    ProductAddPageComponent,
    ProductAddPageReactiveFormComponent,
    ErrorLabelComponent,
    UserAddPageReactiveFormComponent,
    RxJsPageComponent,
    Sibling1Component,
    Sibling2Component,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {component: UserPageComponent, path: 'user'},
      {component: ProductPageComponent, path: 'product', canActivate: []}, // puste canActivate nie blokuje
      {component: TestPageComponent, path: 'test', canActivate: [TestGuard]}, // TestGuard srawdza dostęp
      {component: RxJsPageComponent, path: 'rxjs'},
      {component: ProductAddPageComponent, path: 'product-add'},
      {component: ProductAddPageReactiveFormComponent, path: 'product-add-reactive-form'},
    ]),
    FormsModule, // dodane tylko dla tego aby można było korzystać z ngModel (ProductAddPageComponent)
    ReactiveFormsModule, // dodane aby konrzyać z formularzy reaktywnych (ProductAddPageReactiveFormComponent)
    NgbModule,
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'pl' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
