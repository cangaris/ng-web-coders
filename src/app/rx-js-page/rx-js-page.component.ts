import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-rx-js-page',
  templateUrl: './rx-js-page.component.html',
  styleUrls: ['./rx-js-page.component.css']
})
export class RxJsPageComponent implements OnInit {
  show = false;

  ngOnInit(): void {
    setTimeout(
      () => this.show = true, 10000
    );
  }
}
