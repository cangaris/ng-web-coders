import {Component, Input, OnInit} from '@angular/core';
import {RxJsService} from '../../core/services/rx-js.service';

@Component({
  selector: 'app-sibling2',
  templateUrl: './sibling2.component.html',
  styleUrls: ['./sibling2.component.css']
})
export class Sibling2Component implements OnInit {

  test;

  constructor(private rxJsService: RxJsService) {
  }

  onClick(): void {
    this.rxJsService.setSiblingsSubject('kliknięte 2');
  }

  ngOnInit(): void {
    this.rxJsService.getSiblingsSubject()
      .subscribe(value => this.test = value);
  }
}
