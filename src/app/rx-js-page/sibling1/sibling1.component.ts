import {Component, Output, EventEmitter, OnInit} from '@angular/core';
import {RxJsService} from '../../core/services/rx-js.service';

@Component({
  selector: 'app-sibling1',
  templateUrl: './sibling1.component.html',
  styleUrls: ['./sibling1.component.css']
})
export class Sibling1Component implements OnInit {

  test;

  constructor(private rxJsService: RxJsService) {
  }

  onClick(): void {
    this.rxJsService.setSiblingsSubject('kliknięte 1');
  }

  ngOnInit(): void {
    this.rxJsService.getSiblingsSubject()
      .subscribe(value => this.test = value);
  }
}
